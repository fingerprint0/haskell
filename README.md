# Haskell

This project is just a list of various small pieces of code I've written while learning Haskell. 

# Haskell

This project is just a list of various small pieces of code I've written while learning Haskell. 

## helloworld :: setting up compiling with ghc 

## continuations-nix-cabal :: CPS + Nix 
  * Exploring rewriting functions in CPS instead manually
  * Getting used to working with nix as a package manger
  
## monads :: what are these?
  * examples follow the paper https://cs.uwaterloo.ca/~david/cs442/monads.pdf with some deviations
    to the code they provide
