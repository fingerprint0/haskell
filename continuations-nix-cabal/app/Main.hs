
main :: IO ()
main = mapM_ print [checkFn fact factC 10,
                    checkFn fib fibC 10]

checkFn :: Integral a => (a -> a) -> (a -> (a -> a) -> a) -> a -> Bool
checkFn nc c k = 
  let ncRes = nc k
      cRes = c k id
   in ncRes == cRes

fact :: Integral a => a -> a
fact 0 = 1
fact n = n * fact (n-1)

factC :: Integral a => a -> (a -> a) -> a
factC 0 c = c 1
factC n c = 
  let cont x = c $ n * x
    in factC (n-1) cont

fib :: Integral a => a -> a
fib 0 = 1
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

fibC :: Integral a => a -> (a -> a) -> a
fibC 0 c = c 1 
fibC 1 c = c 1
fibC n c = 
  let cont x = c $ fibC (n-1) id + x
   in fibC (n-2) cont

