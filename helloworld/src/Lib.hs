module Lib
    ( customLast
    ) where

customLast :: [a] -> a 
customLast [x] = x
customLast (_:xs) = customLast xs
