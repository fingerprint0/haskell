import Grammar
import qualified Interps.Identity as Id
import qualified Interps.Error as Err
import qualified Interps.ErrorWPos as ErrP
import qualified Interps.State as State
import qualified Interps.Output as Op

id0 = App 
        (Lam "x" (Add (Var "x") (Var "x")))
        (Add (Con 10) (Con 11))

error0 = App 
          (Lam "x" (Add (Var "x") (Var "x")))
          (Add (Var "y") (Con 11))

error1 = App 
           (Lam "x" (Add (Var "y") (Var "x")))
           (Add (Con 10) (Con 11))

error2 = App (Con 1) (Con 2)

errwpos0 = App 
            (At "Lam0" $ Lam "x" (Add (Var "x") (Var "x")))
            (Add (At "Arg0" $ Con 10) (At "Arg1" $ Con 11))

errwpos1 = App (At "Arg0" $ Con 1) (At "Arg1" $ Con 2)

state0 = Con 10

op0 = App 
        (Lam "x" (Add (Var "x") (Var "x")))
        (Add (Con 10) (Con 11))

testName :: String -> String -> String -> String
testName m t res = "Testing " ++ m ++ " monad" ++ " on test " ++ t ++ ": " ++ res

main :: IO ()
main = do
  putStrLn $ testName "identity" "id0" $ Id.test id0
  putStrLn $ testName "error" "error0" $ Err.test error0
  putStrLn $ testName "errorwpos" "errorwpos0" $ ErrP.test errwpos0
  putStrLn $ testName "errowpos" "errorwpos1" $ ErrP.test errwpos1
  putStrLn $ testName "state" "state0" $ State.test state0 
  putStrLn $ testName "state" "id0" $ State.test id0 
  putStrLn $ testName "state" "error0" $ State.test error0 
  putStrLn $ testName "state" "error1" $ State.test error1 
  putStrLn $ testName "state" "error2" $ State.test error2 
  putStrLn $ testName "output" "id0" $ Op.test op0
