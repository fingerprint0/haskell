module Grammar where 

import qualified Monads.ErrorWPos as ErrP

type Name = String

data Term = Var Name
          | Con Int
          | Add Term Term 
          | Lam Name Term
          | App Term Term
          | At ErrP.Position Term
