module Interps.Error where

import Grammar
import Monads.Error

data Value = Wrong
           | Num Int 
           | Fun (Value -> E Value)

type Environment = [(Name, Value)]

showE :: E Value -> String
showE (Success a) = "Success: " ++ showval a
showE (Error s) = "Error: " ++ s

showval :: Value -> String
showval Wrong = "<wrong>"
showval (Num i) = show i
showval (Fun f) = "<function>"

interp :: Term -> Environment -> E Value
interp (Var x) e = elookup x e
interp (Con i) e = unit (Num i) 
interp (Add t0 t1) e = (interp t0 e) `bind` (\a -> 
                        (interp t1 e) `bind` (\b -> 
                          add a b))
interp (Lam x t) e = unit (Fun (\a -> interp t ((x,a):e)))
-- As both t0 and t1 are interpreted first and then passed to the function
-- This version of the Lambda Calc interpreter uses call-by-value operational semantics
interp (App t0 t1) e = (interp t0 e) `bind` (\f ->
                        (interp t1 e) `bind` (\b ->
                          apply f b))

elookup :: Name -> Environment -> E Value
elookup x [] = Error $ "Variable " ++ x ++ " not found in environemnt"
elookup x ((y,v):rest) = if x==y then unit v else elookup x rest

add :: Value -> Value -> E Value
add (Num i) (Num j) = unit $ Num (i+j)
add _ _ = Error "Trying to add non-numbers"

apply :: Value -> Value -> E Value
apply (Fun k) v = k v
apply _ _  = Error "Trying to apply non-function"

test :: Term -> String
test t = showE $ interp t []
