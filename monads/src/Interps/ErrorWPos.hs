module Interps.ErrorWPos where

import Grammar
import qualified Monads.Error as Err 
import qualified Monads.ErrorWPos as ErrP 

data Value = Wrong
           | Num Int 
           | Fun (Value -> ErrP.P Value)

type Environment = [(Name, Value)]

showE :: Err.E Value -> String
showE (Err.Success a) = "Success: " ++ showval a
showE (Err.Error s) = "Error: " ++ s

showP :: ErrP.P Value -> String
showP f = showE $ f "Start"

showval :: Value -> String
showval Wrong = "<wrong>"
showval (Num i) = show i
showval (Fun f) = "<function>"

interp :: Term -> Environment -> ErrP.P Value
interp (Var x) e = elookup x e
interp (Con i) e = ErrP.unit (Num i) 
interp (Add t0 t1) e = (interp t0 e) `ErrP.bind` (\a -> 
                        (interp t1 e) `ErrP.bind` (\b -> 
                          add a b))
interp (Lam x t) e = ErrP.unit (Fun (\a -> interp t ((x,a):e)))
-- As both t0 and t1 are interpreted first and then passed to the function
-- This version of the Lambda Calc interpreter uses call-by-value operational semantics
interp (App t0 t1) e = (interp t0 e) `ErrP.bind` (\f ->
                        (interp t1 e) `ErrP.bind` (\b ->
                          apply f b))
interp (At p t) e = ErrP.reset p (interp t e) 

elookup :: Name -> Environment -> ErrP.P Value
elookup x [] = ErrP.error $ "Variable " ++ x ++ " not found in environemnt"
elookup x ((y,v):rest) = if x==y then ErrP.unit v else elookup x rest

add :: Value -> Value -> ErrP.P Value
add (Num i) (Num j) = ErrP.unit $ Num (i+j)
add _ _ = ErrP.error "Trying to add non-numbers"

apply :: Value -> Value -> ErrP.P Value
apply (Fun k) v = k v
apply _ _  = ErrP.error "Trying to apply non-function"

test :: Term -> String
test t = showP $ interp t []
