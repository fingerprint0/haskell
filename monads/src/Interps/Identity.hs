module Interps.Identity where

import Grammar
import Monads.Identity

data Value = Wrong
           | Num Int 
           | Fun (Value -> ID Value)

type Environment = [(Name, Value)]

showI :: ID Value -> String
showI = showval

showval :: Value -> String
showval Wrong = "<wrong>"
showval (Num i) = show i
showval (Fun f) = "<function>"

interp :: Term -> Environment -> ID Value
interp (Var x) e = elookup x e
interp (Con i) e = unit (Num i) 
interp (Add t0 t1) e = (interp t0 e) `bind` (\a -> 
                        (interp t1 e) `bind` (\b -> 
                          add a b))
interp (Lam x t) e = unit (Fun (\a -> interp t ((x,a):e)))
-- As both t0 and t1 are interpreted first and then passed to the function
-- This version of the Lambda Calc interpreter uses call-by-value operational semantics
interp (App t0 t1) e = (interp t0 e) `bind` (\f ->
                        (interp t1 e) `bind` (\b ->
                          apply f b))

elookup :: Name -> Environment -> ID Value
elookup x [] = unit Wrong
elookup x ((y,v):rest) = if x==y then unit v else elookup x rest

add :: Value -> Value -> ID Value
add (Num i) (Num j) = unit Num (i+j)
add _ _ = Wrong

apply :: Value -> Value -> ID Value
apply (Fun k) v = k v
apply _ _  = Wrong

test :: Term -> String
test t = showI $ interp t []
