module Interps.Output where

import Grammar
import Monads.Output
import qualified Monads.Error as Err

data Value = Wrong
           | Num Int 
           | Fun (Value -> O Value)

type Environment = [(Name, Value)]

showO :: O Value -> String
showO (Err.Error s) = "Expression failed on error: " ++ s 
showO (Err.Success t) = "Expression evaluated succesfully to " ++ showval (snd t) ++ 
                          " with msg: \n" ++ (fst t)

showval :: Value -> String
showval Wrong = "<wrong>"
showval (Num i) = show i
showval (Fun f) = "<function>"

interp :: Term -> Environment -> O Value
interp (Var x) e = (elookup x e) `bind` (\t -> let s = "Var " ++ x ++ " = " ++ showval t
                                                in Err.unit(s,t))
interp (Con i) e = unit (Num i) `bind` (\t -> let s = "Detected constant : " ++ showval t 
                                               in Err.unit (s,t))
interp (Add t0 t1) e = (interp t0 e) `bind` (\a -> 
                        (interp t1 e) `bind` (\b -> 
                          add a b))
interp (Lam x t) e = unit (Fun (\a -> interp t ((x,a):e))) `bind` 
                      (\t -> let s = "Detected lambda" 
                              in Err.unit (s,t))
-- As both t0 and t1 are interpreted first and then passed to the function
-- This version of the Lambda Calc interpreter uses call-by-value operational semantics
interp (App t0 t1) e = (interp t0 e) `bind` (\f ->
                        (interp t1 e) `bind` (\b ->
                          apply f b))

elookup :: Name -> Environment -> O Value
elookup x ((y,v):rest) = if x==y then unit v else elookup x rest
elookup x [] = errorO $ "Variable " ++ x ++ " not found in environment!"

add :: Value -> Value -> O Value
add (Num i) (Num j) = (unit $ Num (i+j)) `bind` 
                        (\t -> let s = showval (Num i) ++ " + " ++ showval (Num j) 
                                in Err.unit (s, t))
add _ _ = errorO "Adding non-numbers!"

apply :: Value -> Value -> O Value
apply (Fun k) v = (k v) `bind` 
                    (\t -> let s = "Apply " ++ showval (Fun k) ++ " to " ++ showval v
                            in Err.unit (s, t))
apply _ _  = errorO "Incorrect function application!"

test :: Term -> String
test t = showO $ interp t []
