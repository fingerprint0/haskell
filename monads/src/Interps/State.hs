module Interps.State where

import Grammar
import qualified Monads.Error as Err
import qualified Monads.State as State

data Value = Wrong
           | Num Int 
           | Fun (Value -> State.S Value) 

type Environment = [(Name, Value)]

showE :: Err.E (Value, State.State) -> String
showE (Err.Success (v,s)) = "Success: " ++ showval v ++ " in " ++ show s ++ " reductions."
showE (Err.Error s) = "Error: " ++ s

showS :: State.S Value -> String
showS f = showE $ f 0

showval :: Value -> String
showval Wrong = "<wrong>"
showval (Num i) = show i
showval (Fun f) = "<function>"

interp :: Term -> Environment -> State.S Value
interp (Var x) e = elookup x e
interp (Con i) e = State.unit (Num i) 
interp (Add t0 t1) e = (interp t0 e) `State.bind` (\a -> 
                        (interp t1 e) `State.bind` (\b -> 
                          add a b))
interp (Lam x t) e = State.unit (Fun (\a -> interp t ((x,a):e)))
-- As both t0 and t1 are interpreted first and then passed to the function
-- This version of the Lambda Calc interpreter uses call-by-value operational semantics
interp (App t0 t1) e = (interp t0 e) `State.bind` (\f ->
                        (interp t1 e) `State.bind` (\b ->
                          apply f b))

elookup :: Name -> Environment -> State.S Value
elookup x ((y,v):rest) = if x==y then State.unit v else elookup x rest
elookup x [] = State.get `State.bind` 
                (\s -> State.error $ "Could not find var " ++ x ++ " in environment!")

add :: Value -> Value -> State.S Value
add (Num i) (Num j) = State.tick `State.bind` (\() -> State.unit (Num (i+j)))
add _ _ = State.get `State.bind` (\s -> State.error "Trying to add two non numbers!")

apply :: Value -> Value -> State.S Value
apply (Fun k) v = State.tick `State.bind` (\() -> k v)
apply a b  = State.get `State.bind` 
              (\s -> State.error "Applying incorrectly, either not func or not value!")

test :: Term -> String
test t = showS $ interp t []
