module Monads.Error where

data E a = Success a 
         | Error String

unit :: a -> E a
unit a = Success a

bind :: E a -> (a -> E b) -> E b
bind (Success a) f = f a
bind (Error e) f = Error e 

error :: String -> E a
error s = Error s

