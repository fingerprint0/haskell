module Monads.ErrorWPos where

import qualified Monads.Error as MErr

type Position = String 

type P a = Position -> MErr.E a

showpos :: Position -> String
showpos p = "(@" ++ p ++ ") "

unit :: a -> P a
unit a = \p -> MErr.unit a

bind :: P a -> (a -> P b) -> P b
bind f g = \p -> MErr.bind (f p) (\x -> g x p)

error :: String -> P a
error s = \p -> MErr.error (showpos p ++ s) 

reset :: Position -> P a -> P a
reset q f = \p -> f q 
