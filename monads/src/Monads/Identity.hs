module Monads.Identity where

type ID a = a

unit :: a -> ID a
unit a = a

bind :: ID a -> (a -> ID b) -> ID b
bind a k = k a


