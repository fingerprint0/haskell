module Monads.Output where 

import Grammar
import qualified Monads.Error as Err

type O a = Err.E (String, a)

str :: (String, a) -> String
str t = fst t

val :: (String, a) -> a 
val t = snd t

unit :: a -> O a
unit a = Err.unit ("", a)

bind :: O a -> (a -> O b) -> O b
bind m k = m `Err.bind` (\t -> let s0 = str t
                                   m1 = k (val t)
                                in m1 `Err.bind` (\t -> let s1 = str t
                                                            a1 = val t
                                                         in Err.unit (s0++s1++"\n", a1)))

errorO :: String -> O a
errorO s = Err.error s
