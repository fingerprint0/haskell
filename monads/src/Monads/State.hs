module Monads.State where

import qualified Monads.Error as Err

type State = Int
type S a = State -> Err.E (a, State)

unit :: a -> S a
unit a = \s -> Err.unit (a,s)

bind :: S a -> (a -> S b) -> S b
bind f k = \s0 -> let m = f s0 
                       in Err.bind m (\t -> k (fst t) (snd t))

tick :: S ()
tick = \s -> Err.unit ((), s+1)

get :: S State
get = \s -> Err.unit(s,s)

error :: String -> S a
error s = \s0 -> Err.error $ "(After reduction " ++ show s0 ++ "): " ++ s

